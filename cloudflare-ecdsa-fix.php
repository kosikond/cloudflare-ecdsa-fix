<?php
/**
 * Plugin Name: Cloudflare cURL ECDSA fix
 * Description: Adds ECDSA ciphers for successful Cloudflare's Flexible SSL negotiation in php-curl
 * Author:      Ondra Kosik <dev@kosikond.net>
 * Author URl:  http://gitlab.com/kosikond/cloudflare-ecdsa-fix
 * Version:     0.1.0
 * License:     MIT
 */
 
add_action( 'http_api_curl', 'cloudflare_ecdsa_fix', 10, 1);

function cloudflare_ecdsa_fix( $process ){
    curl_setopt( $process, CURLOPT_SSL_CIPHER_LIST, 'ecdhe_ecdsa_aes_128_sha,ecdhe_rsa_aes_256_sha,ecdhe_rsa_aes_128_sha');
	curl_setopt( $process, CURLOPT_SSL_VERIFYPEER, 'false');
} 
